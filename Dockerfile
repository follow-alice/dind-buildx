FROM docker/buildx-bin:0.14.0 as buildx

FROM golang:1.22-alpine AS ecr-creds-helper
RUN go install github.com/awslabs/amazon-ecr-credential-helper/ecr-login/cli/docker-credential-ecr-login@latest


FROM golang:1.22-alpine AS gcr-creds-helper
RUN go install github.com/GoogleCloudPlatform/docker-credential-gcr@v2
    

FROM docker:cli
COPY --from=buildx /buildx /usr/libexec/docker/cli-plugins/docker-buildx
COPY --from=ecr-creds-helper /go/bin/docker-credential-ecr-login /usr/bin/docker-credential-ecr-login
COPY --from=gcr-creds-helper /go/bin/docker-credential-gcr /usr/bin/docker-credential-gcr
